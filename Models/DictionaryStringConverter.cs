﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MauiBlazorHybridAppDemo.Models
{
    public static class DictionaryStringConverter
    {
        public static string ConvertDictionaryToStorageString(Dictionary<string, string> data)
        {
            // write all the key value pairs in the dictionary to a string of alternating key-value with linebreaks after every value
            StringBuilder stringBuilder = new StringBuilder();
            foreach (KeyValuePair<string, string> pair in data)
            {
                stringBuilder.AppendLine(pair.Key);
                stringBuilder.AppendLine(pair.Value);
            }
            return stringBuilder.ToString();
        }

        public static Dictionary<string, string> ConvertStorageStringToDictionary(string storageString)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(storageString)) return dictionary; // there is no saved data - exit

            using (StringReader stringReader = new StringReader(storageString))
            {
                try
                {
                    string? key = string.Empty;
                    string? value = string.Empty;

                    while (stringReader.Peek() != -1) // keep going as long as long as there is still something that can be read
                    {
                        key = stringReader.ReadLine();
                        value = stringReader.ReadLine();

                        if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(value))
                        {
                            // records are only deemed valid and added if both the key and value are not empty or null
                            dictionary.Add(key, value);
                        }
                    }

                    return dictionary;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
    }
}
