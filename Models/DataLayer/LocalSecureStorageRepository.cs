﻿using MauiBlazorHybridAppDemo.Models;
using MauiBlazorHybridAppDemo.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MauiBlazorHybridAppDemo.Models.DataLayer
{
    // note: don't use .Result on any of the SecureStorage funcs or their chains
    // note: you cannot get around this by using .Wait() and then after .Resul
    /// <summary>
    /// Retrieve and store data to local memory of a device
    /// </summary>
    public class LocalSecureStorageRepository : IStorageRepository
    {
        /// <summary>
        /// todo: currently the dictionary is cached as to minimize interaction with storage; possibly further optimize this
        /// the full storage is loaded at startup and used to return stored passwords
        /// however when a new password needs to be stored it writes the entire storage again; this needs to be improved
        /// as of right now the whole dictionary is condensed in 1 string and stored; this needs to be improved, but beware of double keys?
        /// </summary>
        private Dictionary<string, string> storedSecretDicionary = new();
        private readonly static string SECRET_STORAGE_KEY = "SECRET_STORAGE_KEY";

        public async Task<bool> LoadStoredDataIntoCache()
        {
            storedSecretDicionary = await FetchAllStoredSecrets();
            return storedSecretDicionary != null;
        }

        // todo: request an updated list before returning; now just returns the cached list
        public List<SecretEntry> GetAllSecrets()
        {
            List<SecretEntry> entryList = new List<SecretEntry>();

            foreach (KeyValuePair<string, string> item in storedSecretDicionary)
            {
                entryList.Add(new SecretEntry(item.Key, item.Value));
            }

            return entryList;
        }

        // todo: add a warning if default is given
        // todo: only cached values are given, update to sync with db before return
        public string GetSecretWithKey(string key)
        {
            return storedSecretDicionary.GetValueOrDefault(key, string.Empty);
        }

        public Task<bool> UpdateSecretWithKey(string key, string value)
        {
            if (string.IsNullOrEmpty(key) || string.IsNullOrEmpty(value))
            {
                return new Task<bool>(() => { return false; });
            }

            storedSecretDicionary[key] = value;

            return UpdateStoredDictionary(storedSecretDicionary);
        }

        public Task<bool> DeleteSecretWithKey(string key)
        {
            storedSecretDicionary.Remove(key);

            return UpdateStoredDictionary(storedSecretDicionary);
        }

        // sonarlint suggested static
        /// <summary>
        /// Retrieve all records stored in the SecureStorage 
        /// </summary>
        /// <returns>A Dictionary with all the key-value pairs stored</returns>
        private static async Task<Dictionary<string, string>> FetchAllStoredSecrets()
        {
            // SecureStorage only has an Async Get method
            string? retrievedString = await SecureStorage.GetAsync(SECRET_STORAGE_KEY);

            if (string.IsNullOrEmpty(retrievedString))
            {
                return new Dictionary<string, string>();
            }
            else
            {
                return DictionaryStringConverter.ConvertStorageStringToDictionary(retrievedString);
            }
        }

        // sonarlint suggested static
        /// <summary>
        /// Overrides data stored in the SecuredStorage with a stringified version of the parameter
        /// </summary>
        /// <param name="dictionaryToStore">Dictionary to override stored data with</param>
        /// <returns>True if the storign was successful</returns>
        private static async Task<bool> UpdateStoredDictionary(Dictionary<string, string> dictionaryToStore)
        {
            string toStoreData = DictionaryStringConverter.ConvertDictionaryToStorageString(dictionaryToStore);
            if (string.IsNullOrEmpty(toStoreData))
            {
                SecureStorage.Default.Remove(SECRET_STORAGE_KEY);
            }
            else
            {
                await SecureStorage.Default.SetAsync(SECRET_STORAGE_KEY, toStoreData);
            }
            return true;
        }
    }
}
