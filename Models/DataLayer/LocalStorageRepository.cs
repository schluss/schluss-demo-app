﻿using MauiBlazorHybridAppDemo.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MauiBlazorHybridAppDemo.Models.DataLayer
{
    public class LocalStorageRepository : IStorageRepository
    {
        // only possible to be readonly as this is an in-memory db
        private readonly Dictionary<string, string> storedSecretDicionary;

        public LocalStorageRepository()
        {
            storedSecretDicionary = new Dictionary<string, string>();
        }

        public Task<bool> LoadStoredDataIntoCache() { return Task.FromResult(true); }

        public List<SecretEntry> GetAllSecrets()
        {
            List<SecretEntry> entryList = new List<SecretEntry>();

            foreach (KeyValuePair<string, string> item in storedSecretDicionary)
            {
                entryList.Add(new SecretEntry(item.Key, item.Value));
            }

            return entryList;
        }

        //todo: add a warning if default is given
        public string GetSecretWithKey(string key)
        {
            return storedSecretDicionary.GetValueOrDefault(key, string.Empty);
        }

        public Task<bool> UpdateSecretWithKey(string key, string value)
        {
            storedSecretDicionary[key] = value;
            return Task.FromResult(true);
        }

        public Task<bool> DeleteSecretWithKey(string key)
        {
            storedSecretDicionary.Remove(key);
            return Task.FromResult(true);
        }

    }
}
