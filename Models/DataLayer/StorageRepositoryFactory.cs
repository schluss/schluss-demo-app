﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MauiBlazorHybridAppDemo.Models.DataLayer
{
    public static class StorageRepositoryFactory
    {
        // the Factory keeps a singleton reference of the repository and distributes it when asked
        private static IStorageRepository? repoInstance = null;

        public static IStorageRepository GetInstance()
        {
            if (repoInstance == null)
            {
                // for now just return the SecureStorage one alternatively the in-memory variant
                repoInstance = new LocalSecureStorageRepository();
            }

            return repoInstance;
        }
    }
}
