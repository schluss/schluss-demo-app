﻿using MauiBlazorHybridAppDemo.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MauiBlazorHybridAppDemo.Models.DataLayer
{
    public interface IStorageRepository
    {
        Task<bool> LoadStoredDataIntoCache();

        List<SecretEntry> GetAllSecrets();

        string GetSecretWithKey(string key);

        Task<bool> UpdateSecretWithKey(string key, string value);

        Task<bool> DeleteSecretWithKey(string key);
    }
}
