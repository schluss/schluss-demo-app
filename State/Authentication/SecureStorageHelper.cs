﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MauiBlazorHybridAppDemo.State.Authentication
{
	public class SecureStorageHelper
	{
		public async Task SetAsync(string key, string value) => await SecureStorage.Default.SetAsync(key, value);

		public async Task<string> GetAsync(string key) => await SecureStorage.Default.GetAsync(key);

		public bool DeleteKey(string key) => SecureStorage.Default.Remove(key);
	}
}
