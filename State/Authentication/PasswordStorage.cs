﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MauiBlazorHybridAppDemo.State.Authentication
{
	public sealed class PasswordStorage
	{
		private static PasswordStorage instance;
		private readonly SecureStorageHelper secureStorageHelper;

		public PasswordStorage()
		{
			this.secureStorageHelper = new SecureStorageHelper();
		}

		public static PasswordStorage GetInstance()
		{
			return instance == null ? new PasswordStorage() : instance;
		}

		public async Task<bool> IsRegistered()
		{
			return await secureStorageHelper.GetAsync("passwordHash") != null;
		}

        public async Task<bool> RegisterPassword(string password)
		{
			if (!IsValidPassword(password))
			{
				return false;
			}

			string hash = CalculateHash(password);

			await secureStorageHelper.SetAsync("passwordHash", hash);

			return true;
		}

		public async Task<bool> ConfirmPassword(string password)
		{
			if (!IsValidPassword(password))
			{
				return false;
			}

			string storedHash = await secureStorageHelper.GetAsync("passwordHash");

			if (storedHash == null)
			{
				return false; // No password registered
			}

            return storedHash == CalculateHash(password);
		}

		public void DeletePassword()
		{
			secureStorageHelper.DeleteKey("passwordHash");
		}

		public bool IsValidPassword(string password)
		{
			return password.Length == 5 && password.All(char.IsDigit);
		}

		private string CalculateHash(string password)
		{
			// Use a strong hashing algorithm like SHA256
			using (var sha256 = SHA256.Create())
			{
				byte[] hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(password));
				return Convert.ToHexString(hashedBytes);
			}
		}
	}
}
