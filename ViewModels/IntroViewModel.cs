﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using MauiBlazorHybridAppDemo.Models;

namespace MauiBlazorHybridAppDemo.ViewModels
{
	public partial class IntroViewModel
	{
		#region Lifecycle

		public IntroViewModel()
		{
			LoadIntroScreens();
		}

		#endregion

		#region Properties

		public ObservableCollection<CarouselItemModel> IntroScreens { get; set; } = new ObservableCollection<CarouselItemModel>();

		#endregion

		#region Functions

		private void LoadIntroScreens()
		{
			IntroScreens?.Clear();

			IntroScreens.Add(new CarouselItemModel
			{
				Id = 1,
				Title = "Welcome to your own vault",
				Description = "All your data secured and stores safely and easily in your personal vault. You decide what you 'share' ans with whom.",
				Image = "schluss_magnet"
			});

			IntroScreens.Add(new CarouselItemModel
			{
				Id = 2,
				Title = "You are in control",
				Description = "Decide for yourself who gets access to your data, and for how long.",
				Image = "schluss_head"
			});

			IntroScreens.Add(new CarouselItemModel
			{
				Id = 3,
				Title = "Securely stored",
				Description = "Store all your data securely in your vault. Only you have the key. Even Schluss cannot access it.",
				Image = "schluss_key"
			});

			IntroScreens.Add(new CarouselItemModel
			{
				Id = 4,
				Title = "Quick and easy",
				Description = "Share data easily with companies, family and friends.",
				Image = "connections_image"
			});
		}

		#endregion
	}
}
