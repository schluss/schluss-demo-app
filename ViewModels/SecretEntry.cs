﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MauiBlazorHybridAppDemo.ViewModels
{
    public class SecretEntry
    {
        public string? SecretType { get; set; } // todo: create an enum for this
        public string? SecretValue { get; set; } // todo: do something with encryption

        public SecretEntry(string type, string value)
        {
            SecretType = type;
            SecretValue = value;
        }
    }
}
